sudo -u vagrant sed -i s/#force_color_prompt/force_color_prompt/ .bashrc

tar zxvf /vagrant/resources.tgz

service puppet stop
service chef-client stop
update-rc.d puppet disable
update-rc.d chef-client disable

cat resources/vagrantpasswd.txt | passwd vagrant

# Samba Server
apt-get install -y samba

cp -vf resources/smb.conf /etc/samba/smb.conf

service nmbd restart
service smbd restart

cat resources/smbpasswd.txt | smbpasswd -a vagrant

# dnsmasq
apt-get install -y dnsmasq
cp -v resources/dnsmasq_custom.conf /etc/dnsmasq.d/custom.conf
service dnsmasq restart

# polipo
apt-get install -y polipo
cp -v resources/polipo_config /etc/polipo/config
service polipo restart

# nginx
apt-get install -y nginx

rm -rf resources/
sudo -u vagrant sh -c 'echo -n "" > .bash_history'
history -c
